'use strict';
module.exports = function(NumberValidation) {

	NumberValidation.disableRemoteMethod('create', true);
	NumberValidation.disableRemoteMethod('upsert', true);
	NumberValidation.disableRemoteMethod('upsert', true);
	NumberValidation.disableRemoteMethod('exists', true);
	NumberValidation.disableRemoteMethod('findById', true);
	NumberValidation.disableRemoteMethod('find', true);
	NumberValidation.disableRemoteMethod('findOne', true);
	NumberValidation.disableRemoteMethod('destroyById', true);
	NumberValidation.disableRemoteMethod('count', true);
	NumberValidation.disableRemoteMethod('createChangeStream', true);
	NumberValidation.disableRemoteMethod('updateAll', true);
	NumberValidation.disableRemoteMethod('replaceOrCreate', true);
	NumberValidation.disableRemoteMethod('replaceById', true);
	NumberValidation.disableRemoteMethod('prototype.updateAttributes', true);
	NumberValidation.disableRemoteMethod('upsertWithWhere', true);



	function isPrime(value) {
		for (var i = 2; i < value; i++) {
			if (value % i === 0) {
				return false;
			}
		}
		return value > 1;
	}

	NumberValidation.isPrime = function(value, cb) {
		cb(null, isPrime(value));
	};


	NumberValidation.remoteMethod('isPrime', {
		description: 'Blocks a user',
		accepts: [{
			arg: 'value',
			type: 'number',
			description: 'value',
			required: true,
			http: {
				source: 'query'
			}
		}],
		returns: {
			arg: 'isPrime',
			type: 'boolean'
		},
		http: {
			path: '/isPrime',
			verb: 'post',
			status: 200
		}
	});
};