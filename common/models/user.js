'use strict';
var loopback = require('loopback');
var app = require('../../server/server');

module.exports = function(User) {


	User.disableRemoteMethod('upsert', true);
	User.disableRemoteMethod('upsert', true);
	User.disableRemoteMethod('exists', true);
	User.disableRemoteMethod('findById', true);
	User.disableRemoteMethod('find', true);
	User.disableRemoteMethod('findOne', true);
	User.disableRemoteMethod('destroyById', true);
	User.disableRemoteMethod('count', true);
	User.disableRemoteMethod('createChangeStream', true);
	User.disableRemoteMethod('updateAll', true);
	User.disableRemoteMethod('replaceOrCreate', true);
	User.disableRemoteMethod('replaceById', true);
	User.disableRemoteMethod('prototype.updateAttributes', true);
	User.disableRemoteMethod('upsertWithWhere', true);


	//Only admin can create admin
	User.observe('before save', function adminValidation(ctx, next) {

		var currentUser = app.currentUser;

		if (currentUser) {
			if (app.currentUser.role !== 'admin') {
				if (ctx.instance) {
					ctx.instance.__data.role = 'read';
				} else {
					ctx.data.role = 'read';
				}
				next();
			} else {
				next();
			}
		} else {
			//si es el primero se convierte en admin
			User.find({}, function(err, results) {
				if (err) {
					next(err);
				} else {
					if (results.length === 0) {
						ctx.instance.__data.role = 'admin';
					} else {
						if (ctx.instance) {
							ctx.instance.__data.role = 'read';
						} else {
							ctx.data.role = 'read';
						}
					}
				}
			});
			next();
		}
	});

	User.createAdmin = function(data, cb) {
		var ctx = loopback.getCurrentContext();
		var accessToken = ctx && ctx.get('accessToken');

		User.create({
			username: data.username,
			email: data.email,
			password: data.password,
			role: 'admin'
		}, function(err, users) {
			if (err) {
				return cb(err);
			} else {
				cb();
			}
		});
	};


	User.remoteMethod('createAdmin', {
		description: 'Create new admin',
		accepts: [{
			arg: 'data',
			type: {},
			description: 'username, email and password',
			required: true,
			http: {
				source: 'body'
			}
		}],
		returns: {
			arg: 'success',
			type: 'boolean'
		},
		http: {
			path: '/createAdmin',
			verb: 'post',
			status: 200
		}
	});

	User.lockUser = function(email, cb) {
		var currentUser = app.currentUser;
		User.findOne({
			where: {
				email: email
			}
		}, function(err, instance) {
			if (err) {
				cb(err);
			} else {
				if (instance.id.toString() !== currentUser.id.toString()) {
					instance.updateAttributes({
						enabled: false
					}, function(err, instance) {
						if (err) {
							cb(err);
						} else {
							cb();
						}
					});
				} else {
					cb('You can not block yourself');
				}
			}
		});
	};


	User.remoteMethod('lockUser', {
		description: 'Blocks a user',
		accepts: [{
			arg: 'email',
			type: 'string',
			description: 'user email',
			required: true,
			http: {
				source: 'query'
			}
		}],
		returns: {
			arg: 'success',
			type: 'boolean'
		},
		http: {
			path: '/lockUser',
			verb: 'post',
			status: 200
		}
	});

	User.unlockUser = function(email, cb) {
		console.log(email);
		User.findOne({
			where: {
				email: email
			}
		}, function(err, instance) {
			if (err) {
				cb(err);
			} else {
				instance.updateAttributes({
					enabled: true
				}, function(err, instance) {
					if (err) {
						cb(err);
					} else {
						cb();
					}
				});
			}
		});
	};


	User.remoteMethod('unlockUser', {
		description: 'Blocks a user',
		accepts: [{
			arg: 'email',
			type: 'string',
			description: 'user email',
			required: true,
			http: {
				source: 'query'
			}
		}],
		returns: {
			arg: 'success',
			type: 'boolean'
		},
		http: {
			path: '/unlockUser',
			verb: 'post',
			status: 200
		}
	});
};