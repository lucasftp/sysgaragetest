/*jshint strict:false */
'use strict';
var loopback = require('loopback');

module.exports = function(app) {
  var Role = app.models.Role;

  var roleFunctionAdmin = function(role, context, cb) {
    function reject() {
      process.nextTick(function() {
        cb(null, false);
      });
    }

    var currentUser = app.currentUser;
    if (!currentUser) {
      return reject();
    }

    if ((currentUser.role === 'admin') && (currentUser.enabled)) {
      return cb(null, true);
    } else {
      return reject();
    }
  };

   var roleFunctionUnlockUser = function(role, context, cb) {
    function reject() {
      process.nextTick(function() {
        cb(null, false);
      });
    }

    var currentUser = app.currentUser;
    if (!currentUser) {
      return reject();
    }

    if (currentUser.enabled) {
      return cb(null, true);
    } else {
      return reject();
    }
  };

  Role.registerResolver('admin', roleFunctionAdmin);
  Role.registerResolver('unlockUser', roleFunctionUnlockUser);
};
