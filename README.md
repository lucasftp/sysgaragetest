# Detalles #

### Install ###
npm install

### Run ###
"slc run" o "node ."

### Base de datos y tablas ###
La base de datos y las tablas en MySQL se generan en forma automatica debido al archivo **server/boot/automigrates.js** 

### Modelos ###

* user:
Adicionalmente a los metodos por default, permite a los administradores: `crear un administrador`, `bloquear un usuario` (setea la propiedad enabled: false del usuario, y esto hace que el usuario no posea mas el rol "unlockUser". Este rol se utiliza para que solo los usuarios que no esten bloqueados puedan realizar determinadas acciones) y `desbloquear un usuario`.

* numberValidation:
Este modelo posee "dataSource": null, por lo que permite realizar operaciones logicas, pero no impacta en la base de datos. Posee el metodo `isPrime` que permite verificar si un numero es primo.

### Custom end points examples ###

**Create User (el primer usuario creado se convierte automaticamente en admin)**
```sh
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
 \"username\": \"lucas\",
 \"email\": \"perona@medeatec.com\",
 \"password\": \"Lucas2\"
}" "http://localhost:3000/api/users"
```

**Create Admin** (permiso de ejecución: solo admin no bloqueado)
```sh
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" -d "{
 \"username\": \"lucas4\",
 \"email\": \"perona4@medeatec.com\",
 \"password\": \"Lucas2\"
}" "http://localhost:3000/api/users/createAdmin?access_token=rB39NhUciNDVO1Tctq81kisSXtEpl0c7OA3TBd5LFDBe0etG1lmMBI6Ds6EDYOgH"
```

**Lock User** (permiso de ejecución: solo admin no bloqueado)
```sh
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" "http://localhost:3000/api/users/lockUser?email=perona3%40medeatec.com&access_token=rB39NhUciNDVO1Tctq81kisSXtEpl0c7OA3TBd5LFDBe0etG1lmMBI6Ds6EDYOgH"
```

**Unlock User** (permiso de ejecución: solo admin no bloqueado)
```sh
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" "http://localhost:3000/api/users/unlockUser?email=perona3%40medeatec.com&access_token=rB39NhUciNDVO1Tctq81kisSXtEpl0c7OA3TBd5LFDBe0etG1lmMBI6Ds6EDYOgH"
```

**Verficar numero primo** (permiso de ejecución: cualquier usuario no bloqueado)
```sh
curl -X POST --header "Content-Type: application/json" --header "Accept: application/json" "http://localhost:3000/api/numberValidations/isPrime?value=3&access_token=rB39NhUciNDVO1Tctq81kisSXtEpl0c7OA3TBd5LFDBe0etG1lmMBI6Ds6EDYOgH"
```
